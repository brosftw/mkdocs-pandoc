# Dockerfile Reference: https://docs.docker.com/engine/reference/builder/#environment-replacement
FROM python:stretch


RUN apt-get update && \
    apt-get install -y git wget && \
    apt-get install -y fonts-lmodern lmodern pandoc texlive && \
    wget https://github.com/jgm/pandoc/releases/download/2.0.6/pandoc-2.0.6-1-amd64.deb && \
    dpkg -i pandoc-2.0.6-1-amd64.deb && \
    pip install mkdocs-bootstrap==0.1.1 mkdocs-cinder==0.9.4 mkdocs-material==2.2.4 mkdocs-bootswatch==0.4.0 mkdocs==0.16.3 mkdocs-pandoc==0.2.6 && \
    wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.1/dumb-init_1.2.1_amd64 && \
    chmod +x /usr/local/bin/dumb-init

# Reference: https://docs.docker.com/engine/reference/builder/#entrypoint
# We need to run this script as root to probably change the UID and GID after we will switch to the mc-server user.

# Usage:
# ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
# CMD ["bash", "-c", "do-some-pre-start-thing && exec my-server"]

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD ["python3"]
