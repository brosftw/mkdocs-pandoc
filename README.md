# mkdocs-pandoc
## GitLab CI/CD Example:
Ein GitLab CI/CD Job Example.
```yaml
pages:
  image: brosftw/mkdocs-pandoc
  before_script:
    ## Add your custom theme if not inside a theme_dir
    ## (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
    ## - apt-get update
    ## - pip install XXX
  script:
  - mkdocs build
  - mkdir pdf
  - for file in docs/werkstatt/*.md; do pandoc -V geometry:margin=1in -V papersize:a4paper --highlight-style pygments -f markdown+grid_tables+table_captions -o pdf/${file:15:-3}.pdf ${file} ; done
  - for file in docs/*.md; do pandoc -V geometry:margin=1in -V papersize:a4paper --highlight-style pygments -f markdown+grid_tables+table_captions -o pdf/${file:5:-3}.pdf ${file} ; done
  - mv site public
  - mv pdf public/pdf
  artifacts:
    paths:
    - public
  only:
  - master


solutions:
  image: brosftw/mkdocs-pandoc
  before_script:
    ## Add your custom theme if not inside a theme_dir
    ## (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
    ## - apt-get update
    ## - pip install XXX
  script:
  - mkdir solutions
  - for file in docs/solutions/*.md; do pandoc -V geometry:margin=1in -V papersize:a4paper --highlight-style pygments -f markdown+grid_tables+table_captions -o solutions/${file:14:-3}.pdf ${file} ; done
  artifacts:
    paths:
    - solutions
  only:
  - master


documentation:
  image: brosftw/mkdocs-pandoc
  before_script:
    ## Add your custom theme if not inside a theme_dir
    ## (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
    ## - apt-get update
    ## - pip install XXX
  script:
  - mkdir werkstatt
  - for file in docs/werkstatt/*.md; do pandoc -V geometry:margin=1in -V papersize:a4paper --highlight-style pygments -f markdown+grid_tables+table_captions -o werkstatt/${file:15:-3}.pdf ${file} ; done
  - for file in docs/*.md; do pandoc -V geometry:margin=1in -V papersize:a4paper --highlight-style pygments -f markdown+grid_tables+table_captions -o werkstatt/${file:5:-3}.pdf ${file} ; done
  artifacts:
    paths:
    - werkstatt
  only:
  - master

```
